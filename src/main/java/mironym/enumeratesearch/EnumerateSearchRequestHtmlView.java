package mironym.enumeratesearch;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.plugin.searchrequestview.AbstractSearchRequestView;
import com.atlassian.jira.plugin.searchrequestview.SearchRequestParams;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;

/**
 * This is the main and only class implementing the logic of the Enumerate
 * Search plugin, see https://bitbucket.org/mironym/enumeratesearch/wiki/Home
 * for more information.
 * 
 * @author mironym
 *
 */
public class EnumerateSearchRequestHtmlView extends AbstractSearchRequestView {
	/**
	 * We need the search service component.
	 */
	private final SearchService searchService;

	/**
	 * We need the application user that is logged in.
	 */
	private final ApplicationUser applicationUser;

	/**
	 * This constructor just uses the ComponentAccessor to get the
	 * {@link #searchService} and {@link #applicationUser}.
	 */
	public EnumerateSearchRequestHtmlView() {
		super();
		this.searchService = ComponentAccessor.getComponent(SearchService.class);
		this.applicationUser = ComponentAccessor.getComponent(JiraAuthenticationContext.class).getLoggedInUser();
	}

	/**
	 * This is the core logic. It uses the search service to get the search
	 * result - the list of issues. It uses the Velocity template
	 * "enumeratesearch.vm" to iterator over it and produce an enumerated for of
	 * query with ready to paste links.
	 * 
	 * @see com.atlassian.jira.plugin.searchrequestview.AbstractSearchRequestView#writeSearchResults(com.atlassian.jira.issue.search.SearchRequest,
	 *      com.atlassian.jira.plugin.searchrequestview.SearchRequestParams,
	 *      java.io.Writer)
	 */
	@Override
	public void writeSearchResults(SearchRequest searchRequest, SearchRequestParams searchRequestParams, Writer writer)
			throws SearchException {
		List<Issue> issues = searchService
				.search(applicationUser, searchRequest.getQuery(), PagerFilter.getUnlimitedFilter()).getIssues();

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("issues", issues);
		params.put("assignees", aggregateByAssignee(issues));
		params.put("unassigned", getUnassigned(issues));

		try {
			writer.write(descriptor.getHtml("enumeratesearch", params));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param issues
	 * @return the collection of the issues that are unassigned.
	 */
	private List<Issue> getUnassigned(List<Issue> issues) {
		List<Issue> result = new ArrayList<Issue>();

		for (Iterator<Issue> iterator = issues.iterator(); iterator.hasNext();) {
			Issue issue = (Issue) iterator.next();

			if (issue.getAssignee() == null) {
				result.add(issue);
			}
		}

		return result;
	}

	/**
	 * @param issues
	 * @return the map User -> List (of assigned Issues to that User), it
	 *         ignores the unassigned issues.
	 */
	private Map<ApplicationUser, List<Issue>> aggregateByAssignee(List<Issue> issues) {
		Map<ApplicationUser, List<Issue>> result = new HashMap<ApplicationUser, List<Issue>>();

		for (Iterator<Issue> iterator = issues.iterator(); iterator.hasNext();) {
			Issue issue = (Issue) iterator.next();

			ApplicationUser user = issue.getAssignee();

			if (user != null) {
				if (!result.containsKey(user)) {
					result.put(user, new ArrayList<Issue>());
				}
				result.get(user).add(issue);
			}
		}
		return result;
	}
}
